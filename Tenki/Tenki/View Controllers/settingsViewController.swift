//
//  settingsViewController.swift
//  Tenki
//
//  Created by Oskar Svensson on 2018-11-08.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import UIKit

class settingsViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var settingsBackroundImage: UIImageView!
    @IBOutlet weak var winterSlider: UISlider!
    @IBOutlet weak var winterLabel: UILabel!
    @IBOutlet weak var animationImageView: UIImageView!
    
    // MARK: - Attributes
    var winterTemp: Float = 0.0

    // MARK: - Functions
    @IBAction func winterSliderValueChanged(_ sender: UISlider) {
        
        let currentValue = Int(sender.value)
        winterLabel.text = "\(currentValue)"
        winterTemp = winterSlider.value
        DataHandler.instance.setTempForWinterClothing(temp: Int(winterTemp))
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        settingsBackroundImage.addSubview(blurEffectView)
        
        var images: [UIImage] = []
            super.viewDidLoad()
            images = [
                UIImage(named: "firework_red0"),
                UIImage(named: "firework_red1"),
                UIImage(named: "firework_red2"),
                UIImage(named: "firework_red3"),
                UIImage(named: "firework_red4"),
                UIImage(named: "firework_red5"),
                UIImage(named: "firework_red6"),
                UIImage(named: "firework_red7")
                ] as! [UIImage]
            animationImageView.animationImages = images
            animationImageView.animationDuration = 0.9
            animationImageView.startAnimating()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let winterClothingValue: Int = DataHandler.instance.getTempForWinterClothing() {
            
            winterSlider.setValue(Float(winterClothingValue), animated: false)
            winterLabel.text = String(winterClothingValue)
            super.viewWillAppear(animated)
            let newImageModel = DataHandler.instance.getTenkiModel()
            self.settingsBackroundImage.image = UIImage(named: newImageModel[0])
        }
    }
}
