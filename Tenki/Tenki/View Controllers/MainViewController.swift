//
//  MainViewController.swift
//  Tenki
//
//  Created by Joakim Brandt on 2018-11-08.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import UIKit
import UserNotifications

class MainViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var backroundImage: UIImageView!
    @IBOutlet weak var tenkiImage: UIImageView!
    
    //MARK: - Attributes
    let api = APIHandler()
    let currentLocation = Location()
    let geoCodingObject = Geocoding()
    
    
    //MARK: - Functions
    override func viewDidLoad() {
        
        super.viewDidLoad()
        Notifications.instance.setContentOfNotifications(body: "Greetings from Tenki!")
        Notifications.instance.sendNotfication(timeInterval: 8)
        currentLocation.locationInit()
        Notifications.instance.sendCalenderNotifications()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        let latitude = String(DataHandler.instance.getLatitude())
        let longitude = String(DataHandler.instance.getLongitude())
        if DataHandler.instance.isLocationChanged() {
            
            api.fetchData(siUnits: true, latitude: latitude, longitude: longitude) { (success) in
                
                if success == true {
                    
                    DataHandler.instance.setIsLocationChanged(false)
                }
                
                self.geoCodingObject.fetchCityNameBasedOnCOORDS() { (locality, error) in
                    
                    if error == nil && locality != nil {
                        
                        let cityName = locality
                        DataHandler.instance.setLocationName(cityName!)
                    }
                        
                    let newTemperature = String(Int(DataHandler.instance.getCurrentWeather().temperature))
                    let newLocation = DataHandler.instance.getLocationName()
                    let newImageModel = DataHandler.instance.getTenkiModel()
                    self.setLabels(temperature: newTemperature, location: newLocation)
                    self.setImages(imageModel: newImageModel)
                }
            }
        } else {
            
            let newTemperature = String(Int(DataHandler.instance.getCurrentWeather().temperature))
            let newLocation = DataHandler.instance.getLocationName()
            let newImageModel = DataHandler.instance.getTenkiModel()
            self.setLabels(temperature: newTemperature , location: newLocation)
            self.setImages(imageModel: newImageModel)
        }
    }
    
    /*--------------------------------------------------------------
     Name: setLabels
     Usage: setLabels(temperature, location)
     ----------------------------------------------------------------
     Updates the label with the current weather data as a string. 
     -----------------------------------------------------------------*/
    func setLabels(temperature: String, location: String) {
        
        DispatchQueue.main.async {
            
            self.temperatureLabel.text = temperature + "°"
            self.locationLabel.text = location
        }
    }
    
    /*--------------------------------------------------------------
     Name: setImages
     Usage: setImages(imageModel)
     ----------------------------------------------------------------
     Updates the background image and the Tenki image depending on
     argument array.
     -----------------------------------------------------------------*/
    func setImages(imageModel: [String]) {

        DispatchQueue.main.async {
            
            self.backroundImage.image = UIImage(named: imageModel[0])
            self.tenkiImage.image = UIImage(named: imageModel[1])
        }
    }
}
