//
//  WeeklyForecastViewController.swift
//  Tenki
//
//  Created by Jonna Lennartsson on 2018-11-08.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import UIKit

class WeeklyForecastViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backroundImage: UIImageView!
    
    // MARK: - Attributes
    var weeklyForecast: [PresentableDay] = []
    
    // MARK: - Functions
    override func viewDidLoad() {
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        backroundImage.addSubview(blurEffectView)
        
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        super.viewWillAppear(animated)
        let newImageModel = DataHandler.instance.getTenkiModel()
        self.backroundImage.image = UIImage(named: newImageModel[0])
        weeklyForecast = DataHandler.instance.getWeeklyForcast()
        tableView.reloadData()
        

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 90
    }
    
    @IBAction func settingsButtonClicked(_ sender: UIButton) {
        
        performSegue(withIdentifier: "SegueToSettings", sender: self)
    }

}
// MARK: - Extensions
extension WeeklyForecastViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return weeklyForecast.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.backgroundColor = .clear
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier:"ForecastTableViewCell", for: indexPath) as? ForecastTableViewCell {
            
            var daily = weeklyForecast[indexPath.row]
            
            if indexPath.row == 0 {
                
                daily.date = "Today"
            } else if indexPath.row == 1 {
                
                daily.date = "Tomorrow"
            }
            
            cell.date.text = daily.date
            cell.temperature.text = daily.avgTemperature
            if (daily.icon == "partly-cloudy-night") {
                
                cell.weatherImage.image = UIImage(named: "cloudy")
            }
            else if (daily.icon == "clear-night") {
                
                cell.weatherImage.image = UIImage(named: "clear-day")
            } else {
                
            cell.weatherImage.image = UIImage(named: daily.icon)
            }
            return cell
            
        } else {
            
            return UITableViewCell()
        }
    }
}
