//
//  Next7Days.swift
//  Tenki
//
//  Created by Oscar Bjurelid on 2018-11-16.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation

struct Next7Days: Codable {
    
    var data: [DailyWeather]
}
