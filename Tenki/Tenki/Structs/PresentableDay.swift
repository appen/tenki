//
//  PresentableDay.swift
//  Tenki
//
//  Created by Oscar Bjurelid on 2018-11-16.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation

struct PresentableDay: Codable {
    
    var summary: String = " "
    var date: String = " "
    var avgTemperature: String = " "
    var icon: String = " "
}
