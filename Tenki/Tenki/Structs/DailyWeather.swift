//
//  DailyWeather.swift
//  Tenki
//
//  Created by Oscar Bjurelid on 2018-11-16.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation

struct DailyWeather: Codable {
    
    var summary: String
    var icon: String
    var apparentTemperatureHigh: Double
    var apparentTemperatureLow: Double
    var time: Double
}
