//
//  currentWeather.swift
//  Tenki
//
//  Created by Max Bekkhus on 2018-11-15.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation

struct CurrentWeather: Codable {
    
    var summary: String = "Life is gud"
    var temperature: Double = 1.0
    var icon: String = "clear-day"
    
    private enum CodingKeys: String, CodingKey {
        
        case summary
        case temperature
        case icon
    }
}
