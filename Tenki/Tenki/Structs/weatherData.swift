//
//  weatherData.swift
//  Tenki
//
//  Created by Max Bekkhus on 2018-11-15.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation

struct WeatherData: Decodable {
    
    var currently: CurrentWeather
    var daily: Next7Days
}
