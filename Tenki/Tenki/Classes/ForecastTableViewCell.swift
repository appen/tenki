//
//  ForecastTableViewCell.swift
//  Tenki
//
//  Created by Max Bekkhus on 2018-11-26.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation
import UIKit

class ForecastTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var date: UILabel!
    
    // MARK: - Functions
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
}
