//
//  TenkiModel.swift
//  Tenki
//
//  Created by Max Bekkhus on 2018-12-01.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation

class TenkiModel {
    
    // MARK: - Attributes
    var tenkiImage: String = "tenki"
    var backgroundImage: String = "clear-day"
    var jacket: String = "Jacket"
    var winterClothing: String = ""
    var sunglasses: String = "Sunglasses"
    var umbrella: String = "Umbrella"
    var model: [String] = ["clear-day", "tenki"]
    
    // MARK: - Functions
    /*--------------------------------------------------------------
     Name: analyzeSettingsAndWeatherDataAndGenerateModel
     Usage: TenkiModel.analyzeSettingsAndWeatherDataAndGenerateModel(Int, CurrentWeather)
     ----------------------------------------------------------------
     Returns a 2-element String array containing names of images to render.
     Return values are based on settings data and current weather data.
     -----------------------------------------------------------------*/
    func analyzeSettingsAndWeatherDataAndGenerateModel(_ settingsValue: Int, _ currentWeather: CurrentWeather ) {
        
        if Int(currentWeather.temperature) <= settingsValue {
            
            winterClothing = "WinterClothing"
        } else {
            
            winterClothing = ""
        }
        
        if currentWeather.icon == "clear-day" || currentWeather.icon == "partly-cloudy-day" {
            
            sunglasses = "Sunglasses"
        } else {
            
            sunglasses = ""
        }
        
        if currentWeather.icon == "rain" {
            
            umbrella = "Umbrella"
        } else {
            
            umbrella = ""
        }
        
        if currentWeather.icon == "sleet" {
            
            umbrella = "Umbrella"
        } else {
            
            umbrella = ""
        }
        
        if currentWeather.icon == "wind" {
            
            backgroundImage = "bg-clear-day"
        } else {
            
            backgroundImage = "bg-" + currentWeather.icon
        }
        
        tenkiImage = "tenki" + jacket + sunglasses + umbrella + winterClothing
        model = [backgroundImage, tenkiImage]
    }
    
    func getModel() -> [String] {
        
        return model
    }
}
