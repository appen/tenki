//
//  DataHandler.swift
//  Tenki
//
//  Created by Max Bekkhus on 2018-11-24.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation
import UserNotifications

class DataHandler {
    
    // MARK: - Attributes
    static let instance = DataHandler()
    private var tempForWinterClothing: Int = 0
    private var latitude: Double = 57.787648
    private var longitude: Double = 14.129789
    private var isLocationSame: Bool?
    private var currentWeather: CurrentWeather = CurrentWeather()
    private var weeklyForcast: WeeklyForcast?
    private var tenkiModel: TenkiModel = TenkiModel()
    private var locationName: String = "Jönköping"
    
    // MARK: - Functions
    // MARK: Settings
    /*--------------------------------------------------------------
     Name: getTempForWinterClothing
     Usage: DataHandler.getTempForWinterClothing()
     ----------------------------------------------------------------
     Returns the set value for when winterclothes will be displayed.
     -----------------------------------------------------------------*/
    func getTempForWinterClothing() -> Int {
        
        if tempForWinterClothing != StorageHandler.getTempForWinterClothing() {
            
            tempForWinterClothing = StorageHandler.getTempForWinterClothing()
        }
        return tempForWinterClothing
    }
    
    /*--------------------------------------------------------------
     Name: setTempForWinterClothing
     Usage: DataHandler.setTempForWinterClothing(temp: Int)
     ----------------------------------------------------------------
     Set the prefered temperature for when to wear winterclothing
     and saves that value to userDefaults.
     -----------------------------------------------------------------*/
    func setTempForWinterClothing(temp: Int) {
        
        self.tempForWinterClothing = temp
        StorageHandler.setTempForWinterClothing(temp)
        self.tenkiModel.analyzeSettingsAndWeatherDataAndGenerateModel(self.tempForWinterClothing, self.currentWeather)
    }
    
    // MARK: Locations
    /*--------------------------------------------------------------
     Name: getLatitude
     Usage: DataHandler.getLatitude()
     ----------------------------------------------------------------
     Returns the latest stored latitude from your device.
     -----------------------------------------------------------------*/
    func getLatitude() -> Double {
        
        if latitude != StorageHandler.getLatitude() {
            
            latitude = StorageHandler.getLatitude()
        }
        return latitude
    }
    
    /*--------------------------------------------------------------
     Name: setLatitude
     Usage: DataHandler.setLatitude(latitude: Double)
     ----------------------------------------------------------------
     Sets the GPS's latitude of your device and saves it in userDefaults.
     -----------------------------------------------------------------*/
    func setLatitude(latitude: Double) {
        
        self.latitude = latitude
        StorageHandler.setLatitude(latitude)
    }
    
    /*--------------------------------------------------------------
     Name: getLongitude
     Usage: DataHandler.getLongitude()
     ----------------------------------------------------------------
     Returns the latest stored longitude from your device.
     -----------------------------------------------------------------*/
    func getLongitude() -> Double {
        
        if longitude != StorageHandler.getLongitude() {
            
            longitude = StorageHandler.getLongitude()
        }
        return longitude
    }
    
    /*--------------------------------------------------------------
     Name: setLongitude
     Usage: DataHandler.setLongitude(longitude: Double)
     ----------------------------------------------------------------
     Sets the GPS's longitude of your device and saves it in userDefaults.
     -----------------------------------------------------------------*/
    func setLongitude(longitude: Double) {
        
        self.longitude = longitude
        StorageHandler.setLongitude(longitude)
    }
    
    /*--------------------------------------------------------------
     Name: getIsLocationChanged
     Usage: DataHandler.getIsLocationChanged()
     ----------------------------------------------------------------
     Returns true if the device has moved. Else false.
     -----------------------------------------------------------------*/
    func getIsLocationChanged() -> Bool {
        
        return isLocationSame!
    }
    
    /*--------------------------------------------------------------
     Name: setIsLocationChanged
     Usage: DataHandler.setIsLocationChanged(Bool)
     ----------------------------------------------------------------
     Sets the bool value describing if the device has changed location.
     -----------------------------------------------------------------*/
    func setIsLocationChanged(_ isLocationChanged: Bool) {
        
        isLocationSame = isLocationChanged
    }
    
    /*--------------------------------------------------------------
     Name: isLocationChanged
     Usage: DataHandler.isLocationChanged()
     ----------------------------------------------------------------
     A confirmation on if location is changed or not.
     -----------------------------------------------------------------*/
    func isLocationChanged() -> Bool {
        
        if isLocationSame == nil {
            
            isLocationSame = true
        }
       return isLocationSame!
    }
    
    // MARK: Current weather
    
    /*--------------------------------------------------------------
     Name: getCurrentWeather
     Usage: DataHandler.getCurrentWeather()
     ----------------------------------------------------------------
     Returns the up-to-date weather in a currentWeather object.
     -----------------------------------------------------------------*/
    func getCurrentWeather() -> CurrentWeather {
        
        if StorageHandler.getCurrentWeather() != nil {
            
            self.currentWeather = StorageHandler.getCurrentWeather()!
        }
        return self.currentWeather
    }
    
    /*--------------------------------------------------------------
     Name: setCurrentWeather
     Usage: DataHandler.setCurrentWeather(CurrentWeather)
     ----------------------------------------------------------------
     Updates the stored weather data and saves it.
     -----------------------------------------------------------------*/
    func setCurrentWeather(_ currentWeather: CurrentWeather) {
        
        self.currentWeather = currentWeather
        StorageHandler.setCurrentWeather(currentWeather)
        self.tenkiModel.analyzeSettingsAndWeatherDataAndGenerateModel(self.getTempForWinterClothing(), self.currentWeather)
    }
    
    //MARK: Weekly forcast
    /*--------------------------------------------------------------
     Name: getWeeklyForcast
     Usage: DataHandler.getWeeklyForcast()
     ----------------------------------------------------------------
     Returns weather data for the upcoming week.
     -----------------------------------------------------------------*/
    func getWeeklyForcast() -> [PresentableDay] {
        
        if self.weeklyForcast?.getWeeklyForcast() == nil {
            
            self.weeklyForcast?.setWeeklyForecast(StorageHandler.getWeeklyForcast()!)
        }
        return (self.weeklyForcast?.getWeeklyForcast())!
    }
    
    /*--------------------------------------------------------------
     Name: setWeeklyForcast
     Usage: DataHandler.setWeeklyForcast(Next7Days)
     ----------------------------------------------------------------
     Updates the stored weather data for the upcoming week and saves it.
     -----------------------------------------------------------------*/
    func setWeeklyForcast(_ weeklyForcast: Next7Days) {
        
        self.weeklyForcast = WeeklyForcast(weeklyForcast)
        StorageHandler.setWeeklyForcast((self.weeklyForcast?.getWeeklyForcast())!)
    }
    
    //MARK: Tenki Model
    /*--------------------------------------------------------------
     Name: getTenkiModel
     Usage: DataHandler.getTenkiModel()
     ----------------------------------------------------------------
     Returns the current model for image rendering.
     -----------------------------------------------------------------*/
    func getTenkiModel() -> [String] {
        
        return self.tenkiModel.getModel()
    }
    
    //MARK: Location Data
    /*--------------------------------------------------------------
     Name: setLocationName
     Usage: DataHandler.setLocationName(locationName)
     ----------------------------------------------------------------
     Updates the stored location name and saves it.
     -----------------------------------------------------------------*/
    func setLocationName(_ locationName: String) {
        
        self.locationName = locationName
        StorageHandler.setLocationName(self.locationName)
    }
    
    /*--------------------------------------------------------------
     Name: getLocationName
     Usage: DataHandler.getLocationName()
     ----------------------------------------------------------------
     Returns saved location name.
     -----------------------------------------------------------------*/
    func getLocationName() -> String {
        
        return StorageHandler.getLocationName()
    }
}
