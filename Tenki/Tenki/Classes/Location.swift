//
//  Location.swift
//  Tenki
//
//  Created by Oscar Bjurelid on 2018-11-08.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation

class Location: NSObject, CLLocationManagerDelegate {
    
    // MARK: - Attributes
    let dispatchGroup = DispatchGroup()
    let locationManager = CLLocationManager()
    var latitude = 0.0
    var longitude = 0.0
    
    // MARK: - Functions
    /*--------------------------------------------------------------
     Name: locationInit()
     Usage: Location.locationInit()
     ----------------------------------------------------------------
     Sets up the locationManager
     -----------------------------------------------------------------*/
    func locationInit(){
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 1000
        locationManager.startUpdatingLocation()
    }
    
    /*--------------------------------------------------------------
     Name: locationManger
     Usage: Location.locationManager(CLLocationManager, [CLLocation])
     ----------------------------------------------------------------
     Updates the saved location in userDefaults when the device has been moved.
     -----------------------------------------------------------------*/
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locationValue:CLLocationCoordinate2D = manager.location!.coordinate
        DataHandler.instance.setLatitude(latitude: locationValue.latitude)
        DataHandler.instance.setLongitude(longitude: locationValue.longitude)
        DataHandler.instance.setIsLocationChanged(true)
    }
    
    func getLocationMan() -> CLLocationManager {
        
        return locationManager
    }
    
    func isAuthorizationStatusDetermined() -> Bool {
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            
            return false
        }
        return true
    }
}
