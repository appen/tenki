//
//  StorageHandler.swift
//  Tenki
//
//  Created by Max Bekkhus on 2018-11-24.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation

class StorageHandler {
    
    // MARK: - Attributes
    static private let encoder = JSONEncoder()
    static private let decoder = JSONDecoder()
    static private let arrayEncoder = PropertyListEncoder()
    static private let arrayDecoder = PropertyListDecoder()
    
    // MARK: - Functions
    
    /*--------------------------------------------------------------
     Name: getTempForWinterClothing
     Usage: StorageHandler.getTempForWinterClothing()
     ----------------------------------------------------------------
     Returns the saved value for when to wear winter clothes from userDefaults.
     -----------------------------------------------------------------*/
    static func getTempForWinterClothing() -> Int {
        
        return UserDefaults.standard.integer(forKey: "tempForWinterClothing")
    }
    
    /*--------------------------------------------------------------
     Name: setTempForWinterClothing
     Usage: StorageHandler.setTempForWinterClothing(temp)
     ----------------------------------------------------------------
     Sets the value for when to wear winter clothes to userDefaults.
     -----------------------------------------------------------------*/
    static func setTempForWinterClothing(_ temp: Int) {
        
        UserDefaults.standard.set(temp, forKey: "tempForWinterClothing")
    }
    
    /*--------------------------------------------------------------
     Name: getLatitude
     Usage: StorageHandler.getLatitude()
     ----------------------------------------------------------------
     Returns the saved latitude.
     -----------------------------------------------------------------*/
    static func getLatitude() -> Double {
        
        return UserDefaults.standard.double(forKey: "latitude")
    }
    
    /*--------------------------------------------------------------
     Name: setLatitude
     Usage: StorageHandler.setLatitude(latitude)
     ----------------------------------------------------------------
     Sets the current latitude for the device and saves it to userDefaults.
     -----------------------------------------------------------------*/
    static func setLatitude(_ latitude: Double) {
        
        UserDefaults.standard.set(latitude, forKey: "latitude")
    }
    
    /*--------------------------------------------------------------
     Name: getLongitude()
     Usage: StorageHandler.getLongitude()
     ----------------------------------------------------------------
     Returns the saved longitude.
     -----------------------------------------------------------------*/
    static func getLongitude() -> Double {
        
        return UserDefaults.standard.double(forKey: "longitude")
    }
    
    /*--------------------------------------------------------------
     Name: setLongitude
     Usage: StorageHandler.setLongitude(longitude)
     ----------------------------------------------------------------
     Sets the current longitude for the device and saves it to userDefaults.
     -----------------------------------------------------------------*/
    static func setLongitude(_ longitude: Double) {
        
        UserDefaults.standard.set(longitude, forKey: "longitude")
    }
    
    /*--------------------------------------------------------------
     Name: getCurrentWeather
     Usage: StorageHandler.getCurrentWeather()
     ----------------------------------------------------------------
     Returns the saved CurrentWeather object.
     -----------------------------------------------------------------*/
    static func getCurrentWeather() -> CurrentWeather? {
        
        if let savedValue = UserDefaults.standard.object(forKey: "currentWeather") as? Data {
            
            if let valueToReturn = try? decoder.decode(CurrentWeather.self, from: savedValue) {
                
                return valueToReturn
            }
            else {
                
                return nil
            }
        }
        return nil
    }
    
    /*--------------------------------------------------------------
     Name: setCurrentWeather
     Usage: StorageHandler.setCurrentWeather(currentWeather)
     ----------------------------------------------------------------
     Sets the currentWeather object and saves it to userDefaults.
     -----------------------------------------------------------------*/
    static func setCurrentWeather(_ currentWeather: CurrentWeather) {
        
        if let encodedValue = try? encoder.encode(currentWeather) {
            
            UserDefaults.standard.set(encodedValue, forKey: "currentWeather")
        }
    }
    
    /*--------------------------------------------------------------
     Name: getWeeklyForcast
     Usage: StorageHandler.getWeeklyForcast()
     ----------------------------------------------------------------
     Returns an array of the upcoming week forcast.
     -----------------------------------------------------------------*/
    static func getWeeklyForcast() -> [PresentableDay]? {
    
        if let data = UserDefaults.standard.value(forKey: "weeklyForecast") as? Data {
            
            if let weeklyForecast = try? arrayDecoder.decode(Array<PresentableDay>.self, from: data) {
                
                return weeklyForecast
            } else {
                
                return [PresentableDay()]
            }
        }
        return [PresentableDay()]
    }
    
    /*--------------------------------------------------------------
     Name: setWeeklyForcast
     Usage: StorageHandler.setWeeklyForcast(weeklyForcast)
     ----------------------------------------------------------------
     Sets an array of the upcoming week forcast and saves it to userDefaults.
     -----------------------------------------------------------------*/
    static func setWeeklyForcast(_ weeklyForcast: [PresentableDay]) {
        
        if let encodedValue = try? arrayEncoder.encode(weeklyForcast) {
            
            UserDefaults.standard.set(encodedValue, forKey: "weeklyForecast")
        }
    }
    
    static func setLocationName(_ locationName: String) {
        
        UserDefaults.standard.set(locationName, forKey: "locationName")
    }
    
    static func getLocationName() -> String {
        
        return UserDefaults.standard.string(forKey: "locationName") ?? "Jönköping"
    }
}
