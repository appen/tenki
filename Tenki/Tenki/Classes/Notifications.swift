//
//  Notifications.swift
//  Tenki
//
//  Created by Jonna Lennartsson on 2018-12-04.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation
import UserNotifications

class Notifications {
    
    //MARK: - Attributes
    static let instance = Notifications()
    let content = UNMutableNotificationContent()
    let calenderContent = UNMutableNotificationContent()
    var date = DateComponents()
    
    //MARK: - Functions
    init() {
        
        content.title = "Tenki - The weather app"
        content.subtitle = " "
        content.body = " "
        content.sound = UNNotificationSound.default
        content.badge = 1
    }
    
    /*--------------------------------------------------------------
     Name: setContentOfNotifications
     Usage: Notifications.instance.setContentOfNotifications(body: String)
     ----------------------------------------------------------------
     Takes a String and updates the body of the notification
     -----------------------------------------------------------------*/
    func setContentOfNotifications(body: String) {
        
        content.body = body
    }
    
    /*--------------------------------------------------------------
     Name: sendNotfication
     Notifications.instance.sendNotfication(timeInterval: 5)
     ----------------------------------------------------------------
     Takes a double as a time interval and sends a notification
     when the time has passed.
     -----------------------------------------------------------------*/
    func sendNotfication(timeInterval: Double) {
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: false)
        let request = UNNotificationRequest(identifier: "testIdentifier", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    /*--------------------------------------------------------------
     Name: sendCalenderNotifications
     Usage: Notifications.instance.sendCalenderNotifications()
     ----------------------------------------------------------------
     Schedules a notification at a specific time.
     -----------------------------------------------------------------*/
    func sendCalenderNotifications() {
        
        date.hour = 8
        date.minute = 00
        calenderContent.title = "Weather report from Tenki"
        calenderContent.subtitle = "weather right now"
        calenderContent.body = DataHandler.instance.getCurrentWeather().summary
        let trigger = UNCalendarNotificationTrigger(dateMatching: date, repeats: true)
        let request = UNNotificationRequest(identifier: "calenderIdentifier", content: calenderContent, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}
