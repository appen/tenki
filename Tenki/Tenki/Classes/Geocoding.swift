//
//  Geocoding.swift
//  Tenki
//
//  Created by Joakim Brandt on 2018-11-25.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation
import CoreLocation

class Geocoding {
    
    // MARK: - Attributes
    var cityName = String()
    var location = CLLocation()
    
    // MARK: - Functions
    /*--------------------------------------------------------------
     Name: fetchCityNameBasedOnCOORDS
     Usage: Geocoding.fetchCityNameBasedOnCOORDS(completion: (String, Error))
     ----------------------------------------------------------------
     Returns the city name for the current coordinates of the device.
     -----------------------------------------------------------------*/
    func fetchCityNameBasedOnCOORDS(completion: @escaping (String?, Error?) -> ()) {
        
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: DataHandler.instance.getLatitude(), longitude: DataHandler.instance.getLongitude())) { placemarks, error in
            guard let placemark = placemarks?.first, error == nil else {
                
                completion(nil, error)
                return
            }
            completion(placemark.locality, nil)
        }
    }
}
