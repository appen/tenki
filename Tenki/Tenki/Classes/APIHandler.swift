//
//  APIHandler.swift
//  Tenki
//
//  Created by Joakim Brandt on 2018-11-08.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation
import UIKit

class APIHandler {
    
    // MARK: - Functions
    /*----------------------------------------------------------------------------------------------------
     Name: fetchData
     Usage: APIHandler.fetchData(siUnits: Bool, latitude: String, longitude: String, completion: ((Bool, CurrentWeather))
     -----------------------------------------------------------------------------------------------------
     Calls the DarkSky-API and asks for weather data for the device position. If succeeded, that data is saved in userDefaults.
     -----------------------------------------------------------------------------------------------------*/
    func fetchData(siUnits: Bool, latitude: String, longitude: String, completion: ((Bool) -> Void)?) {
        
        var siUnitsSubString = ""
        if siUnits { siUnitsSubString = "?units=si" }
        let secretKey = "29fb8f272e147a99f3ce4687208ceb27"
        let jsonUrl = "https://api.darksky.net/forecast/\(secretKey)/\(latitude),\(longitude)\(siUnitsSubString)"
        
        guard let url = URL(string: jsonUrl) else {
            
            completion?(false)
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            
            guard let data = data else { return }
            
            do {
                
                let decodedData = try JSONDecoder().decode(WeatherData.self, from: data)
                DataHandler.instance.setCurrentWeather(decodedData.currently)
                DataHandler.instance.setWeeklyForcast(decodedData.daily)
                completion?(true)
            } catch let decodeError {
                
                completion?(false)
            }
        }.resume()
    }
}
