//
//  WeeklyForcast.swift
//  Tenki
//
//  Created by Jonna Lennartsson on 2018-11-26.
//  Copyright © 2018 Tenki - The Weather App. All rights reserved.
//

import Foundation

class WeeklyForcast {

    // MARK: - Attributes
    var weeklyForcast: [PresentableDay] = []
 
    // MARK: - Functions
    /*--------------------------------------------------------------
     Name: init
     Usage: WeeklyForcast.init(Next7Days)
     ----------------------------------------------------------------
     Constructs an array containing the next seven days forcast.
     -----------------------------------------------------------------*/
    init(_ data: Next7Days) {
        
        for i in 0...7 {
            
            var newDay = PresentableDay(summary: " ", date: " ", avgTemperature: " ", icon: " ")
            newDay.summary = data.data[i].summary
            newDay.icon = data.data[i].icon
            let temp = Int((data.data[i].apparentTemperatureHigh + data.data[i].apparentTemperatureLow)/2)
            newDay.avgTemperature = String(temp) + "°"
            let date = Date.init(timeIntervalSince1970: data.data[i].time)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM"
            newDay.date = dateFormatter.string(from: date)
            self.weeklyForcast.append(newDay)
        }
    }
    
    /*--------------------------------------------------------------
     Name: getWeeklyForcast
     Usage: weeklyForcast.getWeeklyForcast()
     ----------------------------------------------------------------
     Returns an array with the saved weather data for the next seven days.
     -----------------------------------------------------------------*/
    func getWeeklyForcast() -> [PresentableDay] {
        
        return self.weeklyForcast
    }
    /*--------------------------------------------------------------
     Name: setWeeklyForecast
     Usage: weeklyForcast.setWeklyForecast([PresentableDay])
     ----------------------------------------------------------------
     Sets and saves the weekly forcast array into userDefaults. 
     -----------------------------------------------------------------*/
    func setWeeklyForecast(_ weeklyForecast: [PresentableDay]) {
        
        self.weeklyForcast = weeklyForecast
    }
}
